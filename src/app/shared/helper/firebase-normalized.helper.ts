import {CategoryModel} from '../models/category/category.model';

export function FirebaseNormalizedData(data) {
  return data.map(e => {
    return {
      id: e.payload.doc.id,
      ...e.payload.doc.data(),
    };
  });
}
