import {CategoryModel} from '../category/category.model';

export class ProductModel {
  id?: string;
  name: string;
  price: number;
  categories: CategoryModel[];
  shelfLife: any;
}
