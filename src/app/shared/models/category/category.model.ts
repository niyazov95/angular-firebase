export class CategoryModel {
  id?: string;
  editable?: boolean;
  name: string;
}
