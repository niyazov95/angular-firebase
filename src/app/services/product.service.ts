import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {CategoryModel} from '../shared/models/category/category.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  url = '/product';
  constructor(private _fire: FirebaseService) {}

  getProducts() {
    return this._fire.get(this.url);
  }
  createProduct(dto: CategoryModel) {
    return this._fire.create(this.url, dto);
  }
  updateProduct(dto: CategoryModel) {
    this._fire.update(this.url, dto);
  }
  deleteProduct(dto: CategoryModel) {
    this._fire.delete(this.url, dto);
  }
}
