import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {CategoryModel} from '../shared/models/category/category.model';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  url = '/category';
  constructor(private _fire: FirebaseService) {}

  getCategories() {
    return this._fire.get(this.url);
  }
  createCategory(dto: CategoryModel) {
    return this._fire.create(this.url, dto);
  }
  updateCategory(dto: CategoryModel) {
    this._fire.update(this.url, dto);
  }
  deleteCategory(dto: CategoryModel) {
    this._fire.delete(this.url, dto);
  }
}
