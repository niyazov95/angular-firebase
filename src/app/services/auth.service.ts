import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLogged: Subject<boolean> = new Subject<boolean>();
  constructor() {
    this.isLogged.next(false);
  }

  login(pass: string) {
    const password = 'test';

    pass === password ? this.isLogged.next(true) : this.isLogged.next(false);
  }

  logout() {
    this.isLogged.next(false);
  }
}
