import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  constructor(private firestore: AngularFirestore) {}

  get(url: string) {
    return this.firestore.collection(url).snapshotChanges();
  }
  create(url: string, dto: any) {
    return this.firestore.collection(url).add(dto);
  }
  update(url: string, dto: any) {
    // delete dto.id;
    this.firestore.doc(`${url}/${dto.id}`).update(dto);
  }
  delete(url: string, dto: any) {
    this.firestore.doc(`${url}/${dto.id}`).delete();
  }
}
