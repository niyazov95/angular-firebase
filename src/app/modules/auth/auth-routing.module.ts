import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {LocationGuard} from '../../shared/common/location.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    canActivate: [LocationGuard],
    children: [],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
