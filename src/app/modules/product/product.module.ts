import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProductRoutingModule} from './product-routing.module';
import {ProductWrapperComponent} from './product-wrapper.component';
import {SharedModule} from '../shared/shared.module';
import {ProductListComponent} from './components/product-list/product-list.component';
import {MaterialModule} from '../material/material.module';

@NgModule({
  declarations: [ProductWrapperComponent, ProductListComponent],
  imports: [CommonModule, ProductRoutingModule, SharedModule, MaterialModule],
})
export class ProductModule {}
