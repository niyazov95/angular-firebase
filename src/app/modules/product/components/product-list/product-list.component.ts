import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ProductInfoModalComponent} from '../../../shared/components/modals/product-info-modal/product-info-modal.component';
import {ProductModel} from '../../../../shared/models/product/product.model';
import {CategoryModel} from '../../../../shared/models/category/category.model';
import {FirebaseNormalizedData} from '../../../../shared/helper/firebase-normalized.helper';
import {CategoryService} from '../../../../services/category.service';
import {ProductService} from '../../../../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  categories: CategoryModel[];
  products: ProductModel[];
  constructor(private _dialog: MatDialog, private _categoryService: CategoryService, public productService: ProductService) {}

  ngOnInit(): void {
    this.getCategories();
    this.getProducts();
  }

  getCategories() {
    this._categoryService.getCategories().subscribe(data => {
      this.categories = FirebaseNormalizedData(data);
    });
  }

  getProducts() {
    this.productService.getProducts().subscribe(data => {
      this.products = FirebaseNormalizedData(data);
    });
  }

  handleProductModal(data?: ProductModel) {
    const dialog = this._dialog.open(ProductInfoModalComponent, {
      width: '768px',
    });
    dialog.componentInstance.product = data;
  }
}
