import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-product-wrapper',
  template: `
    <div class="container-fluid mt-5">
      <div class="row">
        <div class="col-md-6">
          <app-category-list></app-category-list>
        </div>
        <div class="col-md-6">
          <app-product-list></app-product-list>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class ProductWrapperComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
