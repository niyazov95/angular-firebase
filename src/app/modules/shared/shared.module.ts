import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryListComponent} from './components/category-list/category-list.component';
import {MaterialModule} from '../material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProductInfoModalComponent } from './components/modals/product-info-modal/product-info-modal.component';

@NgModule({
  declarations: [CategoryListComponent, ProductInfoModalComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule],
  exports: [CategoryListComponent],
})
export class SharedModule {}
