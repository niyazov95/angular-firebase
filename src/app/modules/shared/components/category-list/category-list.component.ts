import {Component, OnInit} from '@angular/core';
import {FirebaseNormalizedData} from '../../../../shared/helper/firebase-normalized.helper';
import {CategoryService} from '../../../../services/category.service';
import {CategoryModel} from '../../../../shared/models/category/category.model';
import {ProductService} from '../../../../services/product.service';
import {ProductModel} from '../../../../shared/models/product/product.model';
import {MatDialog} from '@angular/material/dialog';
import {ProductInfoModalComponent} from '../modals/product-info-modal/product-info-modal.component';

@Component({
  selector: 'app-category-list',
  template: `
    <mat-card class="example-card">
      <mat-card-header>
        <div mat-card-avatar class="example-header-image"></div>
        <mat-card-title>Categories</mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <mat-list>
          <mat-list-item *ngFor="let category of categories">
            <div class="d-flex align-items-center justify-content-between w-100">
              <mat-form-field>
                <input [disabled]="!category.editable" type="text" matInput [(ngModel)]="category.name" />
              </mat-form-field>
              <div class="d-flex align-items-center ml-2">
                <button (click)="update(category)" mat-icon-button>
                  <mat-icon color="primary">{{ category.editable ? 'save' : 'edit' }}</mat-icon>
                </button>
                <button (click)="delete(category)" mat-icon-button>
                  <mat-icon color="warn">delete</mat-icon>
                </button>
              </div>
              <mat-divider></mat-divider>
            </div>
          </mat-list-item>
        </mat-list>
      </mat-card-content>
      <mat-card-actions>
        <mat-form-field>
          <mat-label>Category name</mat-label>
          <input [(ngModel)]="categoryName" type="text" matInput />
          <button (click)="create()" color="primary" matSuffix mat-icon-button><mat-icon>save</mat-icon></button>
        </mat-form-field>
      </mat-card-actions>
    </mat-card>
  `,
  styles: [
    `
      mat-card-content {
        height: 500px;
        overflow-y: auto;
        overflow-x: hidden;
      }
      mat-form-field {
        width: 100%;
      }
    `,
  ],
})
export class CategoryListComponent implements OnInit {
  categoryName: string;
  private products: ProductModel[];
  categories: CategoryModel[];
  constructor(private _categoryService: CategoryService, private _productService: ProductService, private _dialog: MatDialog) {}

  ngOnInit(): void {
    this.getProducts();
    this.getCategories();
  }

  private getProducts() {
    this._productService.getProducts().subscribe(data => {
      this.products = FirebaseNormalizedData(data);
    });
  }

  getCategories() {
    this._categoryService.getCategories().subscribe(data => {
      this.categories = FirebaseNormalizedData(data);
    });
  }

  create() {
    const dto = {
      name: this.categoryName,
    };
    this._categoryService.createCategory(dto).then(r => (this.categoryName = ''));
  }

  update(category: CategoryModel) {
    category.editable = !category.editable;
    this._categoryService.updateCategory(category);
  }

  delete(category: CategoryModel) {
    this._categoryService.deleteCategory(category);
    const pinnedProducts = this.products.filter(p => p.categories.find(c => c.id === category.id));
    pinnedProducts.forEach(prod => {
      const modal = this._dialog.open(ProductInfoModalComponent);
      modal.componentInstance.product = prod;
    });
  }
}
