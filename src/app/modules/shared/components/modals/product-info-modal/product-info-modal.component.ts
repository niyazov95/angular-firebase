import {Component, Input, OnInit} from '@angular/core';
import {ProductModel} from '../../../../../shared/models/product/product.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryModel} from '../../../../../shared/models/category/category.model';
import {ProductService} from '../../../../../services/product.service';
import {FirebaseNormalizedData} from '../../../../../shared/helper/firebase-normalized.helper';
import {CategoryService} from '../../../../../services/category.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-product-info-modal',
  templateUrl: './product-info-modal.component.html',
  styleUrls: ['./product-info-modal.component.scss'],
})
export class ProductInfoModalComponent implements OnInit {
  @Input() product: ProductModel;
  categories: CategoryModel[];
  productForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private categoryService: CategoryService,
    private dialogRef: MatDialogRef<ProductInfoModalComponent>
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.getCategories();
  }

  buildForm() {
    if (this.product) {
      this.productForm = this.fb.group({
        name: [this.product.name, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
        price: [this.product.price, [Validators.required, Validators.min(0)]],
        categories: [null, [Validators.required]],
        shelfLife: [new Date(), [Validators.required]],
      });
    } else {
      this.productForm = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
        price: [null, [Validators.required, Validators.min(1)]],
        categories: [null, [Validators.required]],
        shelfLife: [new Date(), [Validators.required]],
      });
    }
  }

  handleSaveForm() {
    this.productForm.patchValue({shelfLife: new Date(this.productForm.get('shelfLife').value).getTime()});
    if (this.product) {
      this.productService.updateProduct({id: this.product.id, ...this.productForm.value});
      this.dialogRef.close();
    } else {
      this.productService.createProduct(this.productForm.value).then(r => {
        this.productForm.reset();
        this.dialogRef.close();
      });
    }
  }

  getCategories() {
    this.categoryService.getCategories().subscribe(data => {
      this.categories = FirebaseNormalizedData(data);
    });
  }

  filterDates = (d: Date | null): boolean => {
    const day = (d || new Date()).getTime();
    return day >= new Date().getTime();
  };
}
